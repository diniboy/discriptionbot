
# DiscriptionBot

Teszt bot a fejlesztői szakkör által fejlesztve

## Telepítés

* [Python 3.6](https://www.python.org/downloads/), illetve a [Git](https://git-scm.com/downloads) legyen telepítve (a telepítések után újra **kell** indítani a gépet!)
* Hozzunk létre egy mappát (bárhol lehet), ahol majd a forráskódot fogjuk tárolni, majd a mappán belül nyomjunk jobbklikket, s válasszuk a _Git Bash Here_ opciót
* A felugró terminálba jobbklikk és _Paste_ segítségével másoljuk be az alábbi parancsot (_a Ctrl+V itt nem működik, ahogy a Ctrl+C-nek is más a dolga_):
```
git clone https://bitbucket.org/diniboy/discriptionbot
```
* Ezt követően, - az így létrejött discriptionbot mappán belül - egy üres helyen nyomjunk Shift+jobbklikk-et! A felugró listában pedig válasszuk a PowerShell-ablak megnyitása itt opciót! (_régebbi Windowsokon ez Parancssor nevet viselt_)
    * Ide illesszük a következő parancsot: ```pip install -r requirements.txt``` (_Ez a parancs telepíti a projektünkhöz szükséges összes hozzávalót, amit a Python alapból nem tartalmaz (pl.: [discord.py](https://github.com/Rapptz/discord.py))_)
    * Majd másoljuk át a minta konfig fájlt az "éles" helyére: ```cp .\config.py.example .\config.py```
 * Szerkesszük a bot tokenjét (_melyett [itt](https://discordapp.com/developers/applications/me) lehet beszerezni_) a config.py fájlba, aztán mentsük le!
 * Kész! :) A bot innentől indítható a mappában található start.bat fájlra kattintva!

## Hasznos oldalak

* [RegExr](https://regexr.com/)
* [JSON megtekintő](http://jsonviewer.stack.hu/)
* [Python 3 dokumentáció](https://docs.python.org/3/)
* [discord.py API dokumentáció](http://discordpy.readthedocs.io/en/latest/api.html)
* [Visual Studio Code](https://code.visualstudio.com/)
* [Stack Overflow](https://stackoverflow.com/questions/tagged/python) - Kiváló hely infógyűjtésre/kérdezni
* [Google](https://www.google.com/) - Bizony, nem szégyen a használata :D
* [discord.py fejlesztői Discord szerver](https://discord.gg/r3sSKJJ) - Ha kérdés van, a srácok készséggel válaszolnak, érdemes csatlakozni :)

### Have fun!
----------
_Dini_

