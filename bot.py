import discord
from discord.ext import commands
import asyncio, async_timeout
import aiohttp
import re
import random
import utils

try:
    import config
except ModuleNotFoundError as e:
    raise ModuleNotFoundError('You have to create a config.py file (just rename the config.py.example), and specify a token there!\nError: ' + str(e))

description = 'Devhub\n\nCsak egy teszt bot'
bot = commands.Bot(command_prefix='!', description=description)
images = []

@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('-' * 6)
    await bot.change_presence(game=discord.Game(name='Under hard development'), status=discord.Status('dnd'))
    bot.session = aiohttp.ClientSession()

@bot.command()
async def ping(ctx):
    "Returns 'Pong!'"
    await ctx.send(':ping_pong: Pong!')

@bot.group()
async def image(ctx):
    'Image getter commands'
    if ctx.invoked_subcommand is None:
        await ctx.send('`Available subcommands are: imgur`')

@image.command()
async def imgur(ctx):
    "Retrieves a random image from earthporn's channel"
    if len(images) == 0:
        await ctx.channel.trigger_typing()
        async with bot.session.get('https://imgur.com/r/earthporn/new/') as req:
            if req.status == 200:
                resp = await req.text()
                items = re.findall('<img alt="(?:[^"]+)?" src="([^"]+)" \\/>', resp)
                for image in items:
                    images.append(image.replace('//', 'https://').replace('b.jpg', '.jpg'))
            else:
                ctx.send('Cannot fetch the images...')
    await ctx.send(embed=utils.formatImage('Imgur kép', random.choice(images), color=utils.gen_random_color(), footer_text='Powered by imgur',
     footer_icon_url='https://raw.githubusercontent.com/DamienDennehy/Imgur.API/master/icon.png'))

@bot.group()
async def gif(ctx):
    if ctx.invoked_subcommand is None:
        await ctx.send('`Available subcommands are: random`')
        return

@gif.command(name='random')
async def _random(ctx):
    async with bot.session.get('http://api.giphy.com/v1/gifs/random?api_key=' + config.GIPHY_TOKEN) as req:
        if req.status == 200:
            json_data = await req.json()
            try:
                await ctx.send(embed=utils.formatImage('Random GIF', json_data['data']['images']['source']['url'], color=utils.gen_random_color(),
                 footer_text='Powered by giphy', footer_icon_url='https://giphy.com/static/img/giphy_logo_square_social.png'))
            except KeyError:
                await ctx.send('**Error!** The server returned an empty response!')
        else:
            await ctx.send('**Error!** No results!')

@bot.command(enabled=False)
async def calc(ctx, *, operation : str):
    try:
        async with async_timeout.timeout(5):
            await ctx.send('A műveleted eredménye: {}'.format(eval(operation)))
    except asyncio.TimeoutError:
        await ctx.send(':x: Túl sok ideig tartott a parancs, le lett állítva!')

bot.run(config.token, reconnect=True)