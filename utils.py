import discord
import random

def formatImage(title : str, image : str, color : hex=discord.Embed.Empty, footer_text : str=None, footer_icon_url : str=None):
    embed = discord.Embed(title=title, color=color)
    embed.set_image(url=image)
    if not footer_icon_url is None and not footer_text is None:
        embed.set_footer(text=footer_text, icon_url=footer_icon_url)
    return embed

def gen_random_color():
   return discord.Color(random.randint(0x000000, 0xFFFFFF))

